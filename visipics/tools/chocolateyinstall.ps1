﻿$ErrorActionPreference = 'Stop';

$packageName= 'visipics'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$url	    = Join-Path $toolsDir "VisiPics-1-31.exe"


$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url

  softwareName  = 'visipics*'

  checksum      = 'c8f927015c6fcdbba3863b4e65c8b4c18670923d85f11e91118be0ae6cacce01'
  checksumType  = 'sha256'

  silentArgs    = '/VERYSILENT'
  validExitCodes= @(0, 3010, 1641)
}

Install-ChocolateyPackage @packageArgs


















